from django.shortcuts import render
from django.http.response import HttpResponse

from datetime import datetime
from django.conf import settings
from jinja2 import Template

def _get_template(templ, param):
    t = Template(templ)
    return t.render(param)

def talk(request):
    query = request.GET['query'] if 'query' in request.GET else 'None'
    param = {'query': query}
    templ = 'Hi , you are welcome, your query is {{query}}.'
    body = _get_template(templ, param)
    return HttpResponse(body,content_type="text/plain")

def text(request):
    body = 'text line'
    return HttpResponse(body,content_type="text/plain")

def hello(request):
    return render(request,'hello/hello.html')

def index(request):
    d_now = datetime.now()
    params = { 
        'timestamp': d_now.strftime(settings.CONF.TIME_FORMAT) ,
        'project_name': settings.CONF.PROJECT_NAME,
    }   
    return render(request, 'hello/index.html', params)

