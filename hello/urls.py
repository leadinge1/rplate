from django.urls import path ## I can't work on this url dispatch with django.urls.path
from django.conf.urls import url ## FIXME url/pathの違い

from hello import views

urlpatterns = [
    path('', views.hello, name='hello'),        ## /hello
    url(r'^index/$', views.index, name='index'), ## /hello/index
    url(r'^text/$', views.text, name='text'),    ## /hello/text
    url(r'^text/talk/$', views.talk, name='talk'),  ## /hello/text/talk
]
