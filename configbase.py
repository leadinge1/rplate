#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Base Config setting
 - 共通のconfig定数をconfigs.default.pyに設定する。
   CONFに読み込まれる。
 - マシン環境(本番、デバック, etc）によって異なる定数の場合
   configs.config_<env.EXECUTION_MODE>.pyに、値を設定する。
   ENVに読み込まれる。
"""

import importlib
import os

__version__='0.0.3'
__date__='18-08-23'

# module statement ------------------------------
from configs import default as CONF  ## Common Config

def _get_environ(env_code):
    rtn = os.getenv(env_code,CONF.DEFAULT_ENV_VALUE[env_code] )
    return rtn.strip()

class Config():
    """ configs配下のconfig_xxx.pyよりConfigを読み込むClass"""
    def __init__(self,env=None):
        if env is None:
            suffix = _get_environ('EXECUTION_MODE')
        else:
            suffix = env
        
        self.ENV = _get_conf('configs.config' + '_' + suffix) ## Environment Config
        self.LOG_CONF = 'logging' + '_' + suffix + ".conf"

def _get_conf(config_name):
    conf = importlib.import_module(config_name)
    return conf


ENV = Config().ENV
LOG_CONF = Config().LOG_CONF

###### debug ---------------------------------###
# configs/config_debug.pyのMODULE_FILEを確認
def process(env):
    print(__doc__)
    print('CONF is "{}" .'.format(CONF.__file__))
    print('ENV  is "{}" .'.format(ENV.__file__))

if __name__ == '__main__' :
    process('debug')

