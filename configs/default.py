import os
MODULE_FILE = os.path.abspath(__file__)
PROJECT_NAME = "REST Service on Web API "
#SERVICE_MODE = 'MOCK'
SERVICE_NAME_SET  = ['postcode']
SERVICE_SCHEMA = {
    'postcode' : {
        'SERVICE_METHOD' : ['GET','POST'],
        'SERVICE_SET' : ['postcode',],
        'BASE_URI' : 'zipcloud.ibsnet.co.jp/api',
        'SERVICE_PATTERN' : 'search',
        },
    }
REQUEST_SCHEMA = {
    'query' : {
        'type' : 'string',
        'maxlength' : 2048 , 
        'empty' : False ,
        'required' : True , 
        },
}
#DOC_DEF = {
#    'postcode' : {
#        'index' : 'postcode-index',
#        'doc_type' : 'postcode',
#        'setting_py' : 'configs.setting_postcode',
#        'mapping_py' : 'configs.mapping_postcode',
#        },
#    }

## --------------------------------------------------
## DUMMY DATA
##
DUMMY_QUERY = 'zipcode=0790177'
DUMMY_REQUEST = {
    "request" : '{"zipcode":"0790177"}'
    }
DUMMY_RESULT =  {
    "message": null,
    "results": [
        {
            "address1": "北海道",
            "address2": "美唄市",
            "address3": "上美唄町協和",
            "kana1": "ﾎｯｶｲﾄﾞｳ",
            "kana2": "ﾋﾞﾊﾞｲｼ",
            "kana3": "ｶﾐﾋﾞﾊﾞｲﾁｮｳｷｮｳﾜ",
            "prefcode": "1",
            "zipcode": "0790177"
        },
        {
            "address1": "北海道",
            "address2": "美唄市",
            "address3": "上美唄町南",
            "kana1": "ﾎｯｶｲﾄﾞｳ",
            "kana2": "ﾋﾞﾊﾞｲｼ",
            "kana3": "ｶﾐﾋﾞﾊﾞｲﾁｮｳﾐﾅﾐ",
            "prefcode": "1",
            "zipcode": "0790177"
        },
        {
            "address1": "北海道",
            "address2": "美唄市",
            "address3": "上美唄町",
            "kana1": "ﾎｯｶｲﾄﾞｳ",
            "kana2": "ﾋﾞﾊﾞｲｼ",
            "kana3": "ｶﾐﾋﾞﾊﾞｲﾁｮｳ",
            "prefcode": "1",
            "zipcode": "0790177"
        }
    ],
    "status": 200
}


## -------- base -----------------------------------------------
TIME_FORMAT = '%Y%m%d_%H%M%S_%f'

DEFAULT_ENV_VALUE = {
    'APPS_DATA' : '',
    'EXECUTION_MODE' : 'debug'
}
TEMPLATE_PTN = '{{message}}'

