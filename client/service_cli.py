import logging.config
import sys

def process(service_path, service_category,query,uri):
    logger = logging.getLogger(__name__)
    logger.debug('// -------------------------------- start ')
    logger.debug(service_path)
    logger.debug(service_category)
    logger.debug(query)
    logger.debug(uri)
    els = CEls.CElasticsearch(uri)
    result = els.search(service_category,query)
    return result
    logger.debug('// -------------------------------- end ')
    
if __name__ == '__main__':
    logging.config.fileConfig('../logging_debug.conf')
    logger = logging.getLogger(__name__)
    service_path = '/anken-index/anken/_search'
    service_category = 'ANKEN'
    query = '{"query":{"match":{"skill_required.lang":"css"}}}'
    uri = 'localhost:9200'
    sys.path.append('..')
    from client import CElasticsearch as CEls
    ret = process(service_path, service_category, query,uri)
    logger.debug(f'ret={ret}')
else:
    from client import CElasticsearch as CEls

