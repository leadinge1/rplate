#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Elasticsearch Operation Class
''' 
import sys
import logging.config
import configbase as base
ENV = base.ENV
CONF = base.CONF

from elasticsearch import Elasticsearch
from pathlib import Path
import importlib
import json

#  -----------------------------------------  #
# class

def _get_param(data_flag):
    index = ''    # index name for elastic
    doc_type = '' # doc_type name for elastic
    
    if data_flag not in CONF.DOC_DEF:
        msg=f'"{data_flag}" is not CONF.DOC_DEF.'
        raise ValueError(msg)
    else:
        index=CONF.DOC_DEF[data_flag]['index']
        doc_type=CONF.DOC_DEF[data_flag]['doc_type']
    
    return index, doc_type

def _get_json(path):
    logger = logging.getLogger(__name__)
    p = Path.cwd()
    build_p = p / path
    logger.debug(build_p)
    
    try:
        with open(build_p, 'r', encoding='utf-8') as f:
            dta = json.load(f)
        
        return dta
    
    except Exception as ex:
        logger.exception(ex)
        raise ex

def _get_conf(config_name):
    conf = importlib.import_module(config_name)
    return conf.schema

class CElasticsearch():
    
    def __init__(self,uri):
        # 起動
        self.es = Elasticsearch(uri)

    def insert_json(self,file_path,data_flag):
        logger = logging.getLogger(__name__)
        # ドキュメントの登録
        docs =[]
        index, doc_type = _get_param(data_flag)
        logger.debug(f'params={file_path}, {data_flag}, {index}, {doc_type}')
        
        docs = _get_json(file_path)
        
        for idx, doc in enumerate(docs):
            logger.debug(idx)
            self.es.index(index=index, doc_type=doc_type, body=doc, id=idx)

    def delete(self,data_flag):
        try:
            index, doc_type = _get_param(data_flag)
            self.es.indices.delete(index=index)
        except:
            pass

    def index_create(self,data_flag):
        try:
            index, doc_type = _get_param(data_flag)
            self.es.indices.delete(index=index)
            setting_py = CONF.DOC_DEF[data_flag]['setting_py']
            mapping_py = CONF.DOC_DEF[data_flag]['mapping_py']
            
            settings = _get_conf(setting_py)
            mapping  = _get_conf(mapping_py)
            
            # settingsを指定してインデックスを作成
            self.es.indices.create(index=index, body=settings)
            
            # 作成したインデックスのマッピングを指定
            es.indices.put_mapping(index=index, doc_type=doc_type, body=mapping)
            
        except:
            pass

    def search(self,data_flag,query):
        logger = logging.getLogger(__name__)
        index, doc_type = _get_param(data_flag)
        result=self.es.search(index=index, doc_type=doc_type, body=query)["hits"]
        logger.debug(result)
        return result['hits']

#  -----------------------------------------  #
# main process
#

def process(file_path,data_flag):
    logger = logging.getLogger(__name__)
    
    try:
        logger.info("//  Begin   import process ----------------------------- >>")
        uri = ENV.ELASTICSEARCH_URI
        esi = CElasticsearch(uri)
        esi.insert_json(file_path,data_flag)
        logger.info("//  Finish  import process ----------------------------- <<")
    
    except Exception as ex:
        logger.exception(ex)
        sys.exit(1)

if __name__ == '__main__':
    logging.config.fileConfig('logging_debug.conf')
    file_path='test_data/result10.txt~~'
    process(file_path,'ANKEN')
