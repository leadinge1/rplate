import logging.config
from cerberus import Validator
from jinja2 import Template
import urllib.parse as urlparse

def merge_dicts(x,y):
    merged = x.copy()
    merged.update(y)
    return merged

def add_dicts(x,y,prop='result'):
    x[prop] = y 
    return x

def get_template(templ,param):
    t = Template(templ)
    return t.render(param)

def validate(data_map,data_schema):
    logger = logging.getLogger(__name__)
    try:
        v = Validator(data_schema)
        validated = v.validate(data_map)
        if validated:
            return data_map
        else:
            msg = v.errors
            raise ValueError(msg)
    except Exception as ex:
        logger.exception(ex)
        raise(ex)

def normalize(data_map,data_schema):
    logger = logging.getLogger(__name__)
    try:
        v = Validator(data_schema)
        normalized = v.normalize(data_map)
        logger.debug(normalized)
        if normalized:
            return normalized
        else:
            msg = v.errors
            raise ValueError(msg)
    except Exception as ex:
        logger.exception(ex)
        raise(ex)

def get_service_path(service_category,service_name):
    base_uri=CONF.SERVICE_SCHEMA[service_category]['BASE_URI']
    service_path=CONF.SERVICE_SCHEMA[service_category]['SERVICE_PATTERN']
    spath = urlparse.urljoin(base_uri, service_path)
    return spath

def get_service_category(service_name):
    service_category=''
    if service_name == 'postcode':
        service_category = 'postcode'
    else: 
        raise ValueError(f'not supported : {service_name}')  

    return service_category

def get_message(service_path, service_category, query):
    logger = logging.getLogger(__name__)
    try:
        # dummy data for test
        dummy_obj = {'service_path' : service_path, 'service_category' : service_category, 'query' : query}
        uri = ENV.SERVER_ADDRESS
        result_obj = cli.process(service_path, service_category, query, uri)
        if service_mode == 'MOCK':
            result_obj = dummy_obj
        #result_str = json.dumps(result_obj,ensure_ascii=False)
        return result_obj
    except Exception as ex:
        logger.exception(ex)
        raise(ex)

def populate(request_map, result_map):
    ret = merge_dicts(request_map,result_map)
    return ret

def generate_query():
    ## FIXME set correct query
    ret = CONF.DUMMY_QUERY
    return ret


def process(request_map,params):
    
    logger = logging.getLogger(__name__)
    global service_mode
    
    ## get config settings
    service_name_set  = CONF.SERVICE_NAME_SET      ## service path name
    service_schema = CONF.SERVICE_SCHEMA
    request_schema = CONF.REQUEST_SCHEMA
    result_dummy  = CONF.DUMMY_RESULT
    
    ## start process ------------------------------------------------
    logger.debug(f'params:{params}')
    logger.debug(f'request_map:{request_map}')
    logger.debug(f'request_schema:"{request_schema}"')
    
    ## transport request
    service_name = params['service_name'] if 'service_name' in params else ''
    last_path = params['last_path'] if 'last_path' in params else ''
    request_json = request_map['request']
    
    ## validate data
    data_map = validate(request_map, request_schema)
    logger.debug('validate ok!!')
    
    ### get service category 
    service_name = last_path if service_name is None else service_name
    logger.debug(f'service_name={service_name}')
    service_category = get_service_category(service_name)
    
    ## create service path
    ### In Case : including in service name set and service_schema or not in
    service_path = ''
    if  service_name in service_name_set: 
        if service_category in service_schema:
            service_path = get_service_path(service_category,service_name)
        else:
            raise ValueError(f'not support {service_category} in {service_name}')
    else:
        raise ValueError(f'not support {service_name}')
    
    logger.debug(f'service_path={service_path}')
    
    ## create query
    query = generate_query(request_json)
    
    ## get message from service
    result_map = get_message(service_path, service_category, query)
    if service_mode == 'MOCK':
        result_map = merge_dicts(result_map, result_dummy)

    ## populate and response
    #return_map = populate(data_map, result_map)
    return_map = result_map
    return return_map

if __name__ == '__main__':
    logging.config.fileConfig('../logging_debug.conf')
    logger = logging.getLogger(__name__)
    import sys
    sys.path.append('..')
    from client import service_cli as cli
    import configbase as base
    ENV = base.ENV
    CONF = base.CONF
    service_mode = ENV.SERVICE_MODE
    module_file  = CONF.MODULE_FILE
    project_name = CONF.PROJECT_NAME
    request_map = CONF.DUMMY_REQUEST
    logger.debug(f'// -- {project_name} ---------------------------service start.')
    logger.debug(f'module_file = {module_file}')
    logger.debug(f'service_mode = {service_mode}')
    params = {'service_name':'search','last_path':'search'}
    ret = process(request_map, params)
    logger.debug(f'return_map="{ret}"')
    logger.debug(f'// -- {project_name} ---------------------------service end.')
else:
    from django.conf import settings
    ENV = settings.ENV
    CONF = settings.CONF
    service_mode = ENV.SERVICE_MODE
    from client import service_cli as cli

