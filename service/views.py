from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from django.conf import settings
CONF = settings.CONF
ENV  = settings.ENV

import logging.config

from service import services as serv

## local method     ----------------------------------

def _get_map(request):
    logger = logging.getLogger(__name__)
    dct = {}
    if request.method == 'GET':
        dct = request.query_params.copy()

    if request.method == 'POST':
        request.POST._mutable = True
        dct = request.data.copy()

    return dct

## Viewset class definition --------------------------
class ServiceView(APIView):
    """
    GET,POST
    """
    def service(self, request_map,service_name,last_path):
        """service: """

        logger = logging.getLogger(__name__)
        logger.debug(f'request_data : {request_map}')
        logger.debug(f'service_name : {service_name}')
        logger.debug(f'last_path : {last_path}')

        try:
            ## call service process
            params = {'service_name' : service_name, 'last_path' : last_path}
            return_result = serv.process(request_map,params)
            return Response(return_result, status=status.HTTP_200_OK)

        except Exception as ex:
            logger.exception(ex)
            return Response({f'error = {ex}'},
                            status=status.HTTP_400_BAD_REQUEST)


    def get(self, request,service_name=None):
        map_data = _get_map(request)
        last_path = request.path.split('/')[-2]
        return self.service(map_data,service_name,last_path)

    def post(self, request, service_name=None):
        map_data = _get_map(request)
        last_path = request.path.split('/')[-2]
        return self.service(map_data,service_name,last_path)

