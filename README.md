# Django Sample Template

## Overview
- Django Web Frameworkを使ったサンプルアプリです。

## Description
- 以下で、urlでアクセス。
|no| url | note |
|--|----|-------|
|1|127.0.0.1:8000/hello|hello world! (html templateをrender)|
|2|127.0.0.1:8000/hello/index|hello world! (parameterでhtml templateを変換して、render）|
|3|127.0.0.1:8000/hello/text|text messageをresponse|
|4|127.0.0.1:8000/hello/text/talk/?query=1234|requestのqueryを取得して、text messageに入れてresponse|


### module list
#### django
|django framework module | description|
|------------------------|------------|
|manage.py               |django manage application|
|db.sqlite3              |NO USE      |
|rest/settings.py        |site settings|
|rest/urls.py            |site url dispach settings|
|rest/wsgi.py            |wsgi execution settings |
|hello/ (apps folder)    |sample web app  |
|service/                |service web app|
|service/views.py        |control and views app|
|service/urls.py         |routining settings |
|service/services.py     |service main module|
|service/models.py       |NO USE (DB MODELS)   |

#### others
|other modules           | description|
|------------------------|------------|
|configbase.py           |config operation for environment  |
|logging_debug.conf      |logging configs |
|configs/default.py      |default configs |
|configs/config_debug.py |configs for environment of debug |

#### elasticsearch client
|elastic modules         | description|
|------------------------|------------|
|client/service_cli.py   |adapter     |
|client/CElasticsearch.py|service client module|

## python environment
```
$ conda create -n rest python=3.6.5
$ conda activate rest
```

* If not change by 'conda activate' , use a command ```$ source activate rest```
* If deactivate from this environment , ```$ conda deactivate```

## Installation

```
(rest) $ pip install -r requirements.txt
```

### package list
- requirements.txt

```
Cerberus==1.2
Django==2.1
djangorestframework==3.8.2
docopt==0.6.2
elasticsearch==6.3.0
gunicorn==19.9.0
Jinja2==2.10
MarkupSafe==1.0
pbr==4.1.0
pytz==2018.5
six==1.11.0
stevedore==1.28.0
urllib3==1.23
virtualenv==16.0.0
virtualenv-clone==0.3.0
virtualenvwrapper==4.8.2
```

## Execution settings

- clone 

```
$ cd ~/pyconle
$ git clone https://leadinge1@bitbucket.org/leadinge1/rplate.git

```
- set your configuration into configs/config_debug.py


## Usage
- change directory ```cd /workspace/py/rplate```

- run ```.run.cmd```

- access
  + /postcode/

```
    http://127.0.0.1:8081/api/v1/postcode/?={"query" : {.....}}
```

// --- end of file
