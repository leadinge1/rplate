from django.contrib import admin
from django.urls import path,include ##<<

urlpatterns = [
    path('api/v1/', include('service.urls')),
    path('hello/', include('hello.urls')),
    path('admin/', admin.site.urls),
]
